package com.example.demo.controller;

import com.example.demo.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PersonController {

    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        // Fetch the list of persons from a service or repository
        List<Person> persons = new ArrayList<>();

        String[] names = {"Able","Boy","Conrad","Demolition","Elize"};
        for(String x : names){
            var p = new Person();
            p.setPersonName(x);
            p.setJoinDate(LocalDateTime.now());
            persons.add(p);
        }
        return persons;
    }
}
